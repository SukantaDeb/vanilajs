"use strict";

let addressData = (function() {
    const addresses = [{
            name: "Sukanta DEv",
            home: "Prodip Dev er bari",
            village: "Chittagong",
            'post office': "Bondor",
            thana: "Bondor",
            district: "Cattogram"
        },
        {
            name: "Mohin Khan",
            home: "Munsi para",
            village: "Chittagong",
            'post office': "Bondor",
            thana: "Bondor",
            district: "Cattogram"
        },
        {
            name: "Sirajul Islam",
            home: "Master Bari",
            village: "Haijdi",
            'post office': "Chaprasirhat",
            thana: "Kabirhat",
            district: "Noakhali"
        }
    ];
    return addresses;
})();
console.log(addressData);

const Address = {};
Address.htmlTable = {
    buildTh: function(column) {
        let theadText = document.createTextNode(column.toUpperCase());
        let th = document.createElement("th");
        th.appendChild(theadText);
        return th;
    },

    buildTableHead: function(titles) {
        let tr = document.createElement("tr");

        for (let title of titles) {
            let th = Address.htmlTable.buildTh(title);
            tr.appendChild(th);
        }
        return tr;
    },

    buildTbody: function(column) {
        let tdCell = document.createTextNode(column);
        let td = document.createElement("td");
        td.appendChild(tdCell);
        return td;
    },

    buildTableBody: function(collection) {

        let tbody = document.createElement('TBODY');
        for (let data of collection) {
            let tr = document.createElement("tr");
            let td = "";
            for (let proparty in data) {

                td = Address.htmlTable.buildTbody(data[proparty]);
                tr.appendChild(td);

            }

            tbody.appendChild(tr);

        }

        return tbody;
    },

    buildTable: function(collection) {

        let table = document.createElement("table");
        let titles = Object.keys(collection[0]);
        let tHead = Address.htmlTable.buildTableHead(titles);
        let tBody = Address.htmlTable.buildTableBody(collection);
        table.appendChild(tHead);
        table.appendChild(tBody);
        // table.border = 1; // trivial way
        table.setAttribute('border', '1'); // Nodes discussion

        return table;
    },

    display: function(addressData, domlocation) {

        let container = document.querySelector(domlocation);
        let table = Address.htmlTable.buildTable(addressData);
        container.appendChild(table);

        return true;
    }

};

Address.htmlTable.display(addressData, '#root');