'use strict'


const data = (function() {

    // const products = [{
    //         id: 1,
    //         type: "Laptop",
    //         brand: "HP",
    //         price: 100000
    //     },
    //     {
    //         id: 2,
    //         type: "Mouse",
    //         brand: "A4",
    //         price: 400
    //     },
    //     {
    //         id: 3,
    //         type: "Keyboard",
    //         brand: "Logitech",
    //         price: 300
    //     }
    // ]
    const animals = [{
            name: "Whiskers",
            species: "cat",
            foods: {
                likes: ["celery", "strawberries"],
                dislikes: ["carrots"]
            }
        },
        {
            name: "Woof",
            species: "dog",
            foods: {
                likes: ["dog food"],
                dislikes: ["cat food"]
            }
        },
        {
            name: "Fluffy",
            species: "cat",
            foods: {
                likes: ["canned food"],
                dislikes: ["dry food"]
            }
        }
    ]

    return animals;
})(); // IIFE : Always one time execute

function xyz(arg1) {

}

let table = document.createElement('table'); // Virtual DOM

for (let animals of data) {

    let itemIdText = document.createTextNode(animals.name);
    let itemIdTd = document.createElement('td');
    itemIdTd.appendChild(itemIdText);

    let itemTypeText = document.createTextNode(animals.species);
    let itemTypeTd = document.createElement('td');
    itemTypeTd.appendChild(itemTypeText);

    let itemBrandText = document.createTextNode(animals.foods.likes);
    let itemBrandTd = document.createElement('td');
    itemBrandTd.appendChild(itemBrandText);

    let itemPriceText = document.createTextNode(animals.foods.dislikes);
    let itemPriceTd = document.createElement('td');
    itemPriceTd.appendChild(itemPriceText);


    let tr = document.createElement('tr'); // find the reason why
    tr.appendChild(itemIdTd);
    tr.appendChild(itemTypeTd);
    tr.appendChild(itemBrandTd);
    tr.appendChild(itemPriceTd);


    table.appendChild(tr);

}

console.log(table);
let container = document.querySelector("#root")
container.appendChild(table);